import axios from "axios";
import useSWR, { useSWRConfig } from "swr";
import { Post, File } from "@prisma/client";
import { ClientPost } from "./client-utils";

const fetcher = (url: string) => axios.get(url).then((res) => res.data);

export const useThread = (plankName: string, id: number, limit?: number) => {
    let url = `/api/${plankName}/threads/${id}`;
    if (limit) {
        url +=`?limit=${limit}`;
    }
    const { data, error, isLoading } = useSWR(url.toString(), fetcher, {
        refreshInterval: !limit ? 1000 : 0,
    });
    return {
        posts: (data ? data["posts"] : []) as ClientPost[],
        isLoading,
        isError: error,
    };
};

export const useMutateThread = (plankName: string, id: number) => {
    const { mutate } = useSWRConfig();
    return async () => {
        await mutate(`/api/${plankName}/threads/${id}`);
    };
};
