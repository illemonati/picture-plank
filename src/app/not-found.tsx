import { ComponentBox } from "./components/ComponentBox";
import { TitleImage } from "./components/TitleImage";
import { notFoundImageURL } from "@/config";

const NotFound = () => {
    return (
        <div>
            <TitleImage />
            <ComponentBox title="404 Not Found" maxW="lg" centerTitle>
                <div className="flex justify-center">
                    <img className="" src={notFoundImageURL}/>
                </div>
            </ComponentBox>
        </div>
    );
};

export default NotFound;
