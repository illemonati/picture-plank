import stc from "string-to-color";
import fontColorContrast from "font-color-contrast";
import { DateTime } from "luxon";
import { ClientPost, getContentURL } from "@/client-utils";
import { FC, useState } from "react";
import { useRouter } from "next/navigation";

interface PostViewProps {
    post: ClientPost;
    i?: number;
    previewMode?: boolean;
    threadId?: number;
    plankName?: string;
}

export const PostView: FC<PostViewProps> = ({
    post,
    i,
    previewMode,
    threadId,
    plankName,
}) => {
    const idBgColor = stc(post.threadMember.id);
    const idFontColor = fontColorContrast(idBgColor);
    const router = useRouter();

    i = i || 0;

    const [big, setBig] = useState(false);
    const [loaded, setLoaded] = useState(false);

    return (
        <div
            className={`gap-2 py-2 ml-7 inline-flex flex-col mr-4 break-words${
                i > 0
                    ? " bg-sfw-bg-reply border-solid border-r border-b border-sfw-border-reply  pl-2"
                    : "max-w-4xl pl-5"
            }`}
            key={post.id}
        >
            <div className="flex gap-1 pr-4 flex-wrap">
                {i > 0 && (
                    <p className="-translate-x-7 text-slate-400">{">>"}</p>
                )}
                <input type="checkbox" />
                {post.subject && (
                    <p className="font-semibold text-subject-text mr-1">
                        {post.subject}
                    </p>
                )}
                <p className="font-semibold text-name-text">
                    {post.name || "Anonymous"}
                </p>
                <p>
                    (ID:
                    <span
                        className="rounded-full px-2 text-sm"
                        style={{
                            backgroundColor: idBgColor,
                            color: idFontColor,
                        }}
                    >
                        {post.threadMember.id}
                    </span>
                    )
                </p>
                <p className="tracking-tighter">
                    {DateTime.fromISO(post.created.toString()).toFormat(
                        "MM/dd/yy(EEE)hh:mm:ss"
                    )}
                </p>
                <p>No.{post.id}</p>
                {i < 1 && previewMode && (
                    <p>
                        [
                        <span
                            className="hover:text-title-text hover:cursor-pointer"
                            onClick={() => {
                                threadId &&
                                    plankName &&
                                    router.push(
                                        `/${plankName}/thread/${threadId}`
                                    );
                            }}
                        >
                            View
                        </span>
                        ]
                    </p>
                )}
            </div>
            <div
                className={`${
                    i > 0 ? "ml-10" : "ml-5"
                } flex flex-row flex-wrap pr-4 `}
            >
                {post.File && (
                    <div className={`${!big && !loaded ? "h-40": ""}`}>
                        <img
                            className={`w-auto mr-4 cursor-pointer ${
                                big ? "" : "max-h-40 max-w-[10rem]"
                            }`}
                            onClick={() => {
                                setBig((b) => !b);
                            }}
                            src={getContentURL(
                                post.File.id,
                                post.File.fileName
                            )}
                            onLoad={() => {setLoaded(true)}}
                        />
                    </div>
                )}
                <div>
                    {post.comment.split("\n").map((line, i) => {
                        return (
                            <p className="leading-5 break-words" key={i}>
                                {line}
                            </p>
                        );
                    })}
                </div>
            </div>
        </div>
    );
};
