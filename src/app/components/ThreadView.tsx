"use client";

import { FC } from "react";
import { useThread } from "@/swr";
import { PostView } from "./PostView";
import { ReactNode } from "react";
import Link from "next/link";

interface ThreadViewProps {
    plankName: string;
    id: number;
    limit?: number;
    previewMode?: boolean;
}

export const ThreadView: FC<ThreadViewProps> = ({
    plankName,
    id,
    limit,
    previewMode,
}) => {
    const { posts, isLoading, isError } = useThread(plankName, id, limit);
    return (
        <div>
            <div className="flex flex-col gap-1">
                {posts.map((post, i) => (
                    <div key={post.id}>
                        <PostView
                            post={post}
                            i={i}
                            previewMode={previewMode}
                            threadId={id}
                            plankName={plankName}
                        />
                    </div>
                ))}
            </div>
        </div>
    );
};
