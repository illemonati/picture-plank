import { FC } from "react";
import { getThreadFirstPost } from "../utils";
import { getContentURL } from "@/client-utils";
import Link from "next/link";

interface ThreadPreviewProps {
    threadId: number;
    plankName: string;
}

export const ThreadPreview: FC<ThreadPreviewProps> = async ({
    threadId,
    plankName,
}) => {
    const result = await getThreadFirstPost(threadId);

    if (!result) {
        return <p>Error</p>;
    }

    const { post, replys } = result;

    return (
        <Link href={`/${plankName}/thread/${threadId}`}>
            <div className="w-40 max-h-80 overflow-hidden flex flex-col items-center  text-center break-words hover:cursor-pointer">
                {post ? (
                    <>
                        <div className="inline-block">
                            {post.File && (
                                <img
                                    className="max-h-40"
                                    src={getContentURL(
                                        post.File.id,
                                        post.File.fileName
                                    )}
                                ></img>
                            )}
                            <p>
                                R: <span className="font-bold">{replys}</span>
                            </p>
                            {post.comment
                                .split("\n")
                                .map((line: string, i: number) => {
                                    return (
                                        <p
                                            className="leading-5 break-words"
                                            key={i}
                                        >
                                            {i === 0 && post.subject && (
                                                <span className="font-bold">
                                                    {post.subject}
                                                    <span className="font-normal">
                                                        :
                                                    </span>
                                                </span>
                                            )}
                                            {line}
                                        </p>
                                    );
                                })}
                        </div>
                    </>
                ) : (
                    <p>Error</p>
                )}
            </div>
        </Link>
    );
};
