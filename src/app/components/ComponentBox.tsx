import { FC, ReactNode } from "react";

interface ComponentBoxProps {
    title?: string;
    centerTitle?: boolean;
    children?: ReactNode,
    maxW?:string
    flexDir?: "col" | "row"
}

export const ComponentBox: FC<ComponentBoxProps> = ({title, centerTitle, maxW, children, flexDir}) => {
    return (
        <div className={`border-solid border border-black max-w-${maxW || '4xl'} container mx-auto bg-box-bg`}>
            {title && <p className={`bg-sfw-bg-title px-2 py-0.5 font-bold text-xl ${centerTitle && 'text-center'}`}>{title}</p>}
            <div className={`md:container md:mx-auto flex justify-evenly flex-${flexDir || "col"}`}>
               {children}
            </div>
        </div>
    );
};
