import { picturePlankURL, planks } from "@/config";
import Link from "next/link";
import { ReactNode, FC } from "react";

interface BracketContentProps {
    children?: ReactNode;
    linkURL?: string;
    linkTarget?: string;
}

const BracketContent: FC<BracketContentProps> = ({
    children,
    linkURL,
    linkTarget,
}) => {
    return (
        <div className="inline-flex">
            <p className="text-slate-400">[</p>
            <div className="flex text-slate-700 w-full flex-row gap-1">
                {linkURL ? (
                    <Link
                        className="hover:text-link-text"
                        target={linkTarget}
                        href={linkURL}
                    >
                        {children}
                    </Link>
                ) : (
                    children
                )}
            </div>
            <p className="text-slate-400">]</p>
        </div>
    );
};

export const TopBottomBar = () => {
    return (
        <div className="flex text-slate-400 w-full">
            <BracketContent>
                {Object.keys(planks).map((plank, i) => (
                    <>
                        <Link
                            className="hover:text-link-text"
                            href={`/${plank}`}
                        >
                            <p>{plank}</p>
                        </Link>
                        {i + 1 < Object.keys(planks).length && <p>/</p>}
                    </>
                ))}
            </BracketContent>

            <div className="flex ml-auto whitespace-nowrap">
                <BracketContent linkURL={picturePlankURL} linkTarget="_blank">
                    Picture Plank
                </BracketContent>
                <BracketContent linkURL="/">Home</BracketContent>
            </div>
        </div>
    );
};
