import { titleImageURL } from "@/config";
import Link from "next/link";

export const TitleImage = () => (
    <div className="flex justify-center flex-row w-full">
        <div className="max-w-md h-36">
            <Link href="/">
                <img
                    src={titleImageURL}
                    alt="title image"
                    className="h-full"
                ></img>
            </Link>
        </div>
    </div>
);
