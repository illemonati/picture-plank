"use client";

import { ReactNode } from "react";
import { FC } from "react";
import Link from "next/link";

interface BracketContentProps {
    children?: ReactNode;
    linkURL?: string;
    linkTarget?: string;
    onClick?: () => any | void;
}

const BracketContent: FC<BracketContentProps> = ({
    children,
    linkURL,
    linkTarget,
    onClick,
}) => {
    return (
        <div className="inline-flex">
            <p className="">[</p>
            <div
                className="flexw-full flex-row gap-1 hover:cursor-pointer hover:text-link-text"
                onClick={onClick ? onClick : () => {}}
            >
                {linkURL ? (
                    <Link className="" target={linkTarget || ""} href={linkURL}>
                        {children}
                    </Link>
                ) : (
                    children
                )}
            </div>
            <p className="">]</p>
        </div>
    );
};

interface ThreadBarProps {
    plankName: string;
    variant: "top" | "bottom";
    plankMode?: boolean;
}

export const ThreadBar: FC<ThreadBarProps> = ({
    plankName,
    variant,
    plankMode,
}) => {
    return (
        <>
            {variant === "bottom" && (
                <hr className="text-black border-t border-hr-border my-2 mx-1" />
            )}
            <div className="flex flex-row gap-1 ml-1">
                {plankMode ? (
                    <>
                        <BracketContent linkURL={`/${plankName}/catalog`}>
                            Catalog
                        </BracketContent>
                    </>
                ) : (
                    <>
                        <BracketContent linkURL={`/${plankName}`}>
                            Return
                        </BracketContent>
                        <BracketContent linkURL={`/${plankName}/catalog`}>
                            Catalog
                        </BracketContent>
                        <BracketContent
                            onClick={() => {
                                window.scrollTo({
                                    top:
                                        variant === "top"
                                            ? document.body.scrollHeight
                                            : 0,
                                    behavior: "smooth",
                                });
                            }}
                        >
                            {variant === "top" ? "Bottom" : "Top"}
                        </BracketContent>
                    </>
                )}
            </div>
            {variant === "top" && (
                <hr className="text-black border-t border-hr-border my-2 mx-1" />
            )}
        </>
    );
};
