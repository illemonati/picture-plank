import "./globals.css";
import type { Metadata } from "next";
import { config } from "../config";

export const metadata: Metadata = {
    title: config.title,
    description: config.description,
};


export default function RootLayout({
    children,
}: {
    children: React.ReactNode;
}) {
    return (
        <html lang="en">
            
            <body className={`bg-sfw-bg w-full relative font-sans`}>
                {children}
                <p className="text-center w-full mt-4">{config.footerMessage}</p>
            </body>
        </html>
    );
}
