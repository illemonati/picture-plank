import { config, translateContentPath, translatePublicPath } from "@/config";
import { NextRequest, NextResponse } from "next/server";
import path from "path";
import fs from "fs";
import { fileTypeFromStream } from "file-type";



export const GET = async (req: NextRequest, {params}: {params: {slug: string[]}}) => {
    const filePath = translateContentPath(path.join(...params.slug), true);
    if (!fs.existsSync(filePath)) {
        return NextResponse.redirect("/404");
    }

    const stream1 = fs.createReadStream(filePath);
    const stream2 = fs.createReadStream(filePath);

    return new NextResponse(stream1 as unknown as ReadableStream, {
        headers: {
            "Content-Type": (await fileTypeFromStream(stream2 as unknown as ReadableStream))?.mime || 'application/octet-stream',
        }
    });
}