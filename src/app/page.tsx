import { titleImageURL } from "../config";
import { config } from "../config";
import Link from "next/link";
import { getStats } from "@/app/utils";
import prettyBytes from "pretty-bytes";
import { TitleImage } from "./components/TitleImage";
import { ComponentBox } from "./components/ComponentBox";

const Home = async () => {
    const { posts, users, activeContent } = await getStats();

    return (
        <div className="flex flex-col gap-y-2">
            <TitleImage />
            <ComponentBox title={config.frontPage.announcement.title} flexDir="col">
                {config.frontPage.announcement.content
                    .split(/\n/)
                    .map((frag, i) => {
                        if (["\n", ""].includes(frag)) {
                            return <br key={i} />;
                        } else {
                            return (
                                <p className="px-2" key={i}>
                                    {frag}
                                </p>
                            );
                        }
                    })}
            </ComponentBox>
            <ComponentBox title="Planks" flexDir="row">
                <div className="md:container md:mx-auto flex justify-evenly gap-5">
                    {Object.keys(config.planks).map((plankGroup, i) => {
                        return (
                            <div key={i} className="flex flex-col">
                                <p className="font-bold underline">
                                    {plankGroup}
                                </p>
                                {Object.keys(config.planks[plankGroup]).map(
                                    (plank, i) => (
                                        <Link href={plank} key={i}>
                                            {
                                                config.planks[plankGroup][plank]
                                                    .name
                                            }
                                        </Link>
                                    )
                                )}
                            </div>
                        );
                    })}
                </div>
            </ComponentBox>
            <ComponentBox title="Stats" flexDir="row">
                <div className="md:container md:mx-auto flex justify-around">
                    <p className="font-bold">Total Posts: {posts}</p>
                    <p className="font-bold">Total Users: {users}</p>
                    <p className="font-bold">
                        Active Content: {prettyBytes(activeContent || 0)}
                    </p>
                </div>
            </ComponentBox>
        </div>
    );
};

export const dynamic = 'force-dynamic';
export const revalidate = 0;

export default Home;
