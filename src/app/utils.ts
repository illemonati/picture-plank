import { prisma } from "../prisma"
import { promisify } from "util"
import fastFolderSize from "fast-folder-size"
import { configDir } from "../config";
import { nanoid } from "nanoid/non-secure";
import path from "path";
const getActiveContentSize = async () => {
    return await promisify(fastFolderSize)(configDir);
}

export const getStats = async () => {
    const posts = await prisma.post.count();
    const users = await prisma.user.count();
    const activeContent = await getActiveContentSize();
    return {posts, users, activeContent};
}

export const generateThreadMemberId = () => {
    return nanoid(7);
}


export const getPlankThreads = async (plankName: string) => {
    return await prisma.thread.findMany({
        orderBy: [
            {
                id: 'desc',
            }
        ],
        where: {
            Plank: {
                id: plankName
            }
        }
    })
}

export const getThreadFirstPost = async (threadId: number) => {
    const posts = await prisma.post.findMany({
        orderBy: [
            {
                id: 'asc'
            }
        ],
        take: 1,
        where: {
            thread: {
                id: threadId,
            }
        },
        include: {
            File: true,
            thread: {
                select: {
                    id: true,
                }
            }
        }
    });

    if (!posts) return null;

    const postsAmount = await prisma.post.count({
        where: {
            thread : {
                id: posts[0].thread.id
            }
        }
    })
    return {
        post: posts[0],
        replys: postsAmount - 1,
    }
}