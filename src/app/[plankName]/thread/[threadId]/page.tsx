
import { config, planks } from "@/config";
import { notFound } from "next/navigation";
import { NewPost } from "../../NewPost";
import { ThreadView } from "@/app/components/ThreadView";
import { ThreadBar } from "@/app/components/ThreadBar";

const Thread = ({
    params,
}: {
    params: { plankName: string; threadId: string };
}) => {
    const { plankName, threadId } = params;
    const plank = planks[plankName];

    if (!plank) {
        return notFound();
    }

    const id = parseInt(threadId);

    return (
        <div>
            <NewPost
                plankName={plankName}
                threadId={parseInt(threadId)}
                maxFileSize={config.maxFileSize}
            />
            <hr className="text-black border-t border-hr-border my-2 mx-1" />
            <hr className="text-black border-t border-hr-border my-2 mx-1" />

            <ThreadBar plankName={plankName} variant="top" />
            <ThreadView plankName={plankName} id={id} />
            <ThreadBar plankName={plankName} variant="bottom"/>
            

            <hr className="text-black border-t border-hr-border my-2 mx-1" />
            <hr className="text-black border-t border-hr-border my-2 mx-1" />
        </div>
    );
};

export default Thread;
