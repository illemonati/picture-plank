import { planks } from "@/config";
import { notFound } from "next/navigation";
import { TopBottomBar } from "../components/TopBottomBar";
import { TitleImage } from "../components/TitleImage";

export default function PlankLayout({
    children,
    params
}: {
    children: React.ReactNode;
    params:  {plankName: string};
}) {
    const { plankName } = params;


    const plank = planks[plankName];

    if (!plank) {
        return notFound();
    }
    return (
        <div>
            <TopBottomBar />
            <TitleImage />
            <p className="text-center text-3xl font-bold text-title-text tracking-[-0.05em]">
                /{plankName}/ - {plank.name}
            </p>
            <hr className="text-black border-t border-hr-border my-2 mx-20"/>
            {children}
            <TopBottomBar />
        </div>
    );
}
