"use client";

import { useMutateThread } from "@/swr";
import axios from "axios";
import { useRouter } from "next/navigation";
import prettyBytes from "pretty-bytes";
import { ChangeEvent, useCallback, useState, FC } from "react";
import { useSWRConfig } from "swr";

interface NewPostProps {
    plankName: string;
    threadId?: number;
    maxFileSize: number;
}

export const NewPost: FC<NewPostProps> = ({ plankName, threadId, maxFileSize }) => {
    const [dialogOpen, setDialogOpen] = useState(false);
    const router = useRouter();
    const mutateThread = useMutateThread(plankName, threadId || 0);

    const subjectObject = threadId ? {} : { subject: "" };

    const [fieldValues, setFieldValues] = useState({
        ...{
            name: "",
            options: "",
        },
        ...subjectObject,
        ...{ comment: "" },
    });

    const [file, setFile] = useState<File | null>(null);

    const handleCreateNewThread = useCallback(() => {
        (async () => {
            const formData = new FormData();
            Object.keys(fieldValues).forEach((fieldName) => {
                const val = fieldValues[fieldName as keyof typeof fieldValues];
                if (!!val) {
                    formData.append(fieldName, val);
                }
            });
            if (file) {
                formData.append('file', file);
            }
            const resp = await axios.post(
                `/api/${plankName}/threads/new`,
                formData,
                { headers: { "Content-Type": "multipart/form-data" } }
            );

            const { threadId } = resp.data;
            router.push(`/${plankName}/thread/${threadId}`);
        })();
    }, [fieldValues, plankName, file, router]);

    const handleCreateNewReply = useCallback(() => {
        (async () => {
            const formData = new FormData();
            Object.keys(fieldValues).forEach((fieldName) => {
                const val = fieldValues[fieldName as keyof typeof fieldValues];
                if (!!val) {
                    formData.append(fieldName, val);
                }
            });
            if (file) {
                formData.append('file', file);
            }
            const resp = await axios.post(
                `/api/${plankName}/threads/${threadId}/reply`,
                formData,
                { headers: { "Content-Type": "multipart/form-data" } }
            );
            const { postId } = resp.data;
            setDialogOpen(false);
            await mutateThread();
            const newURL = new URL(location.href);
            newURL.hash = postId;
            router.push(newURL.toString());
        })();
    }, [fieldValues, plankName, threadId, file, router, mutateThread]);

    const handleChangeFieldValue =
        (fieldName: string) =>
        (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
            setFieldValues((v) => {
                const vCopy = { ...v };
                vCopy[fieldName as keyof typeof fieldValues] = e.target.value;
                return vCopy;
            });
        };

    const handleFileChange = (e: ChangeEvent<HTMLInputElement>) => {
        if (!e.target.files?.length) {
            return;
        }
        console.log(e.target.files);
        const file = e.target.files[0];
        console.log(file);
        if (file.size > maxFileSize) {
            alert(`File size ${prettyBytes(file.size)} larger than ${prettyBytes(maxFileSize)}`)
            e.target.value = "";
            setFile(null);
            return
        }
        setFile(file);
    }

    
    return (
        <div>
            {dialogOpen ? (
                <div className="container mx-auto max-w-md flex flex-col gap-0.5">
                    {Object.keys(fieldValues).map((fieldName, i) => {
                        return (
                            <div
                                className="w-full inline-flex gap-0.5"
                                key={fieldName}
                            >
                                <div className="font-semibold bg-sfw-bg-title w-3/12 border-black border px-2 flex items-center capitalize">
                                    <p>{fieldName}</p>
                                </div>
                                {fieldName === "comment" ? (
                                    <textarea
                                        className="bg-box-bg flex-1 border-input-border border px-2 h-24"
                                        required
                                        value={
                                            fieldValues[
                                                fieldName as keyof typeof fieldValues
                                            ]
                                        }
                                        onChange={handleChangeFieldValue(
                                            fieldName
                                        )}
                                    />
                                ) : (
                                    <input
                                        className="bg-box-bg w-7/12 border-input-border border px-2"
                                        placeholder={
                                            fieldName === "name"
                                                ? "Anonymous"
                                                : ""
                                        }
                                        value={
                                            fieldValues[
                                                fieldName as keyof typeof fieldValues
                                            ]
                                        }
                                        onChange={handleChangeFieldValue(
                                            fieldName
                                        )}
                                    ></input>
                                )}
                                {Object.keys(fieldValues)[i + 1] ===
                                    "comment" && (
                                    <button
                                        className="bg-button-color flex-1 hover:text-title-text  border-black border disabled:opacity-50 disabled:pointer-events-none"
                                        disabled={
                                            fieldValues.comment.length < 1
                                        }
                                        onClick={
                                            threadId
                                                ? handleCreateNewReply
                                                : handleCreateNewThread
                                        }
                                    >
                                        Post
                                    </button>
                                )}
                            </div>
                        );
                    })}
                    <div className=" inline-flex gap-0.5 w-full">
                        <div className="font-semibold bg-sfw-bg-title w-3/12 border-black border px-2 flex items-center capitalize">
                            <p>File</p>
                        </div>

                        <input type="file" className="w-7/12" onChange={handleFileChange}></input>
                    </div>
                </div>
            ) : (
                <p className="text-center text-2xl font-bold">
                    [
                    <span
                        className="hover:text-title-text hover:cursor-pointer"
                        onClick={() => setDialogOpen(true)}
                    >
                        {!threadId ? "Start a New Thread" : "Post a Reply"}
                    </span>
                    ]
                </p>
            )}
        </div>
    );
};
