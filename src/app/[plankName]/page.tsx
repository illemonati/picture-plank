import { TitleImage } from "../components/TitleImage";
import { notFound } from "next/navigation";
import { planks } from "@/config";
import { TopBottomBar } from "../components/TopBottomBar";
import { useState } from "react";
import { NewPost } from "./NewPost";
import { getPlankThreads } from "../utils";
import { ThreadView } from "../components/ThreadView";
import { config } from "@/config";
import { Fragment } from "react";
import { ThreadBar } from "../components/ThreadBar";

export const revalidate = 0;

const plank = async ({ params }: { params: { plankName: string } }) => {
    const { plankName } = params;

    const plank = planks[plankName];

    if (!plank) {
        return notFound();
    }

    const plankThreads = await getPlankThreads(plankName);

    return (
        <div>
            <NewPost plankName={plankName} maxFileSize={config.maxFileSize} />
            <hr className="text-black border-t border-hr-border my-2 mx-1" />
            <hr className="text-black border-t border-hr-border my-2 mx-1" />
            <ThreadBar plankMode variant="top" plankName={plankName} />
            {!!plankThreads.length ? (
                plankThreads.map((thread, i) => {
                    return (
                        <Fragment key={i}>
                            {i > 0 && (
                                <hr className="text-black border-t border-hr-border my-2 mx-2" />
                            )}
                            <ThreadView
                                plankName={plankName}
                                id={thread.id}
                                limit={5}
                                previewMode
                            />
                        </Fragment>
                    );
                })
            ) : (
                <p className="text-center font-bold text-lg my-40">
                    No Threads Yet. Start One?
                </p>
            )}
            <hr className="text-black border-t border-hr-border my-2 mx-1" />
            <hr className="text-black border-t border-hr-border my-2 mx-1" />
        </div>
    );
};

export default plank;
