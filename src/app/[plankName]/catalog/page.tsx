import { ThreadBar } from "@/app/components/ThreadBar";
import { ThreadPreview } from "@/app/components/ThreadPreview";
import { getPlankThreads } from "@/app/utils";
import { NewPost } from "../NewPost";
import { config } from "@/config";

export const revalidate = 0;

const Catalog = async ({ params }: { params: { plankName: string } }) => {
    const { plankName } = params;
    const threads = await getPlankThreads(plankName);
    return (
        <div>
            <NewPost plankName={plankName} maxFileSize={config.maxFileSize} />
            <hr className="text-black border-t border-hr-border my-2 mx-1" />
            <hr className="text-black border-t border-hr-border my-2 mx-1" />

            <ThreadBar plankName={plankName} variant="top" />
            <div className="mx-32 flex gap-10 flex-wrap my-10">
                {threads.map((thread) => {
                    return (
                        <ThreadPreview threadId={thread.id} key={thread.id} plankName={plankName} />
                    );
                })}
            </div>
            <ThreadBar plankName={plankName} variant="bottom" />
            <hr className="text-black border-t border-hr-border my-2 mx-1" />
            <hr className="text-black border-t border-hr-border my-2 mx-1" />
        </div>
    );
};

export default Catalog;
