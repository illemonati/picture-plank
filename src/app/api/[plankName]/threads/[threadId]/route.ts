import { prisma } from "@/prisma";
import { NextRequest, NextResponse } from "next/server";

export const GET = async (req: NextRequest, {params}: {params: {threadId: string}}) => {
    const {threadId} = params;
    const id = parseInt(threadId);

    if (!id) {
        return NextResponse.json("Invalid Thread", {status: 400});
    }

    const { searchParams } = new URL(req.url)

    const limit = searchParams.get('limit');
    const limitNum = limit ? parseInt(limit): undefined;


    const thread = await prisma.thread.findUnique({
        where: {
            id,
        }, 
        select: {
            posts: {
                take: limitNum,
                select: {
                    id: true,
                    threadMember: {
                        select: {
                            id: true,
                        }
                    },
                    created: true,
                    name: true,
                    options: true,
                    subject: true,
                    comment: true,
                    File: {
                        select: {
                            id: true,
                            fileName: true,
                        }
                    }
                }
            }
        },
    })

    return NextResponse.json(thread);
}