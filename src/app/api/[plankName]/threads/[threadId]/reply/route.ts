import { generateThreadMemberId } from "@/app/utils";
import { prisma } from "@/prisma";
import { config, contentDir } from "@/config";
import { NextRequest, NextResponse } from "next/server";
import { v4 } from "uuid";
import path from "path";
import fs from "fs";
import { fileTypeStream } from "file-type/core";
import { Readable } from "stream";


export const POST = async (req: NextRequest, {params}: {params: {threadId: string}}) => {
    const {threadId} = params;
    const id = parseInt(threadId);


    if (!id) {
        return NextResponse.json("Invalid Thread", {status: 400});
    }

    const thread = await prisma.thread.findUnique({
        where: {
            id,
        }
    })


    if (!thread) {
        return NextResponse.json("Invalid Thread", {status: 400});
    }

    const userCookie = req.cookies.get("userid");
    let user = null;
    if (userCookie) {
        user = await prisma.user.findUnique({
            where: {
                id: userCookie.value,
            },
        });
    }

    if (user === null) {
        return NextResponse.json("Unauthorized", {status: 401});
    }

    const formData = await req.formData();

    const name = formData.get("name") as string|null;
    const options = formData.get("options") as string|null;
    const comment = formData.get("comment") as string|null;

    const file = formData.get("file") as File|null;


    if (!comment) {
        return NextResponse.json("Comment Required", {status: 400});
    }


    let ftStream = null;

    if (!!file) {
        if (file.size > config.maxFileSize) {
            return NextResponse.json("File too big", {status: 400});
        } 
        //@ts-ignore
        ftStream = await fileTypeStream(Readable.fromWeb(file.stream()));
    }



    const threadMember = await prisma.threadMember.upsert({
        where: {
            userId_threadId: {
                userId: user.id,
                threadId: id
            }
        },
        create: {
            id: generateThreadMemberId(),
            threadId: id,
            userId: user.id,
        },
        update: {

        }
    })



    const post = await prisma.post.create({
        data: {
            threadMember: {
                connect: threadMember
            },
            thread: {
                connect: thread,
            },
            name,
            options,
            comment,
            File: file && ftStream && ftStream.fileType?.mime.startsWith("image") ? {
                create: {
                    id: v4(),
                    mime: ftStream.fileType.mime,
                    fileName: file.name
                }
            } : undefined
        },
        select: {
            id: true,
            File: {
                select: {
                    id: true,
                    fileName: true,
                }
            }
        }
    })

    if (post.File && ftStream) {
        const filePath = path.join(contentDir, post.File.id, post.File.fileName);
        fs.mkdirSync(path.dirname(filePath), {recursive: true});
        const w = fs.createWriteStream(filePath);
        ftStream.pipe(w);
    }


    return NextResponse.json({postId: post.id});

};
