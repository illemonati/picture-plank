import { prisma } from "@/prisma";
import { NextRequest, NextResponse } from "next/server";
import { v4 } from "uuid";

export const POST = async (req: NextRequest) => {

    const { userCookie, ip } = await req.json();

    let userExists = false;
    let user = null;

    if (userCookie) {
        user = await prisma.user.findUnique({
            where: {
                id: userCookie.value,
            },
        });
        if (user !== null) {
            userExists = true;
        }
    }

    if (user === null) {
        user = await prisma.user.create({
            data: {
                id: v4(),
            }
        });
    }

    if (ip) {
        await prisma.iP.upsert({
            where: {
                ip,
            },
            create: {
                ip,
            },
            update: {
                users: {
                    connect: {
                        id: user.id,
                    },
                },
            },
        });
    }

    return NextResponse.json({ userId: user.id });
};
