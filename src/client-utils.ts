import path from "path";
import { Post, File } from "@prisma/client";
export const getContentURL = (fileId: string, fileName: string) => {
    return path.join("/pp-content", fileId, fileName);
};

export type ClientPost = Post & {
    threadMember: { id: string };
} & { File: File };
