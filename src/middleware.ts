import { NextRequest, NextResponse } from "next/server";
export async function middleware(req: NextRequest) {
    const resp = NextResponse.next();

    const userCookie = req.cookies.get("userid");
    const url = req.nextUrl;
    url.pathname = '/api/auth';

    const authResp = await fetch(url, {
        method: 'POST',
        body: JSON.stringify({
            userCookie,
            ip: req.ip,
        }),
        headers: {
            'Content-Type': 'application/json',
        }
    });

    const {userId} = await authResp.json();

    resp.cookies.set({
        name: "userid",
        value: userId,
        path: "/",
        expires: new Date(new Date().setFullYear(new Date().getFullYear() + 1))
    });

    return resp;
};

export const config = {
    matcher: [
      /*
       * Match all request paths except for the ones starting with:
       * - api (API routes)
       * - _next/static (static files)
       * - _next/image (image optimization files)
       * - favicon.ico (favicon file)
       */
      '/((?!api|_next/static|_next/image|favicon.ico|pp-public).*)',
    ],
  }