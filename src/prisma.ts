import { PrismaClient } from "@prisma/client";
import { dbURL } from "./config";

export const prisma = new PrismaClient({
    datasources: {
        db: { url: dbURL },
    },
});
