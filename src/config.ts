import fs from "fs";
import path from "path";
import { fileTypeFromBuffer } from "file-type";

interface PlankGroups {
    [key: string]: Planks;
}

interface Planks {
    [key: string]: Plank;
}

interface Plank {
    name: string;
}

interface Config {
    title: string;
    description: string;
    public: {
        basePath: string;
        titleImagePath: string;
        notFoundImagePath: string;
    };
    frontPage: {
        announcement: {
            title: string;
            content: string;
        };
    };
    planks: PlankGroups;
    dbPath: string;
    contentDir: string;
    footerMessage: string;
    maxFileSize: number;
}

export const picturePlankURL = "https://gitlab.com/illemonati/picture-plank";

export const configDir =
    process.env.PICTURE_PLANK_CONFIG_DIR || "./illemonati-wall/";

const translateConfigPath = (subpath: string, resolve?: boolean) => {
    const joined = path.join(configDir, subpath);
    return resolve ? path.resolve(joined) : joined;
};

export const translatePublicPath = (subpath: string, resolve?: boolean) => {
    return translateConfigPath(
        path.join(config.public.basePath, subpath),
        resolve
    );
};

const getPublicURL = (subpath: string) => {
    return path.join("/pp-public", subpath);
};

const configFile = translateConfigPath("./config.json");

export const config: Config = JSON.parse(fs.readFileSync(configFile, "utf-8"));

// const titleImagePath = translateConfigPath(path.join(config.public.basePath, config.public.titleImagePath));
// const titleImageBuffer = fs.readFileSync(titleImagePath);
// const titleImageType = await fileTypeFromBuffer(titleImageBuffer);
// export const titleImageURL = `data:${
//     titleImageType?.mime
// };base64,${titleImageBuffer.toString("base64")}`;

export const titleImageURL = getPublicURL(config.public.titleImagePath);
export const notFoundImageURL = getPublicURL(config.public.notFoundImagePath);

export const dbURL = `file:${translateConfigPath(config.dbPath, true)}`;
export const planks: Planks = {};
Object.assign(planks, ...Object.values(config.planks));

// export const notFoundImage =

export const contentDir = translateConfigPath(config.contentDir);
export const translateContentPath = (subpath: string, resolve?: boolean) => {
    return translateConfigPath(
        path.join(config.contentDir, subpath),
        resolve
    );
};
