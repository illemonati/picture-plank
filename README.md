# Picture Plank

An extensible image board software.

Easily configurable, see folder illemonati-wall for a config sample

All configuration / data storage are recommended to be stored within config folder

Use base.db as the base database file for your sqlite db (copy to desired location, change name, change dbPath in config)

[Live Demo](https://iw.tong.icu)
