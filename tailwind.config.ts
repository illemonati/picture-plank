import type { Config as TailwindConfig } from "tailwindcss";

const config: TailwindConfig = {
    content: [
        "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
        "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
        "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
    ],
    theme: {
        extend: {
            colors: {
                'sfw-bg-start': 'rgb(209,213,238)',
                'sfw-bg-end': 'rgb(238,242,255)',
                'sfw-bg-title': '#9988ee',
                'box-bg': 'white',
                'title-text': '#af0a0f',
                "hr-border": "#b7c5d9",
                "link-text": "#d00",
                "input-border": "#aaa",
                "button-color": "#efefef",
                "name-text": "#117743",
                "subject-text": "#0f0c5d",
                "sfw-bg-reply": "#d6daf0",
                "sfw-border-reply": "#b7c5d9",
            }
        }
    },
    plugins: [],
};
export default config;
