/** @type {import('next').NextConfig} */
const nextConfig = {
    headers: () => [
        {
          source: '/:path*',
          headers: [
            {
              key: 'Cache-Control',
              value: 'no-cache, no-store, max-age=0, must-revalidate',
            }, 
          ],
        },
      ],
}

module.exports = nextConfig
